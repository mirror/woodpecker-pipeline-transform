// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package transform

import (
	"errors"

	"github.com/goccy/go-yaml"
)

var UnsupportedError = errors.New("unsupported pipeline")

type Step struct {
	Name        string   `yaml:"-"`
	Image       string   `yaml:"image,omitempty"`
	Pull        bool     `yaml:"pull,omitempty"`
	Environment []string `yaml:"environment,omitempty"`
	Secrets     Secrets  `yaml:"secrets,omitempty"`
	Commands    []string `yaml:"commands,omitempty"`
	Settings    Settings `yaml:"settings,omitempty"`
	Detach      bool     `yaml:"detach,omitempty"`
	Privileged  bool     `yaml:"privileged,omitempty"`
	Volumes     []string `yaml:"volumes,omitempty"`
	When        *When    `yaml:"when,omitempty"`
}

type Steps []*Step

func (s Steps) MarshalYAML() (interface{}, error) {
	v := make(yaml.MapSlice, len(s))
	for i, step := range s {
		v[i] = yaml.MapItem{
			Key:   step.Name,
			Value: step,
		}
	}
	return v, nil
}

type Service struct {
	Name        string   `yaml:"-"`
	Image       string   `yaml:"image"`
	Pull        bool     `yaml:"pull,omitempty"`
	Environment []string `yaml:"environment,omitempty"`
	Secrets     Secrets  `yaml:"secrets,omitempty"`
	Commands    []string `yaml:"commands,omitempty"`
	Settings    Settings `yaml:"settings,omitempty"`
	Privileged  bool     `yaml:"privileged,omitempty"`
	Volumes     []string `yaml:"volumes,omitempty"`
	When        *When    `yaml:"when,omitempty"`
}

type Services []*Service

func (s Services) MarshalYAML() (interface{}, error) {
	v := make(yaml.MapSlice, len(s))
	for i, step := range s {
		v[i] = yaml.MapItem{
			Key:   step.Name,
			Value: step,
		}
	}
	return v, nil
}

type Workspace struct {
	Base string `yaml:"base"`
	Path string `yaml:"path"`
}

type CloneStep struct {
	Image    string   `yaml:"image,omitempty"`
	Settings Settings `yaml:"settings,omitempty"`
}

type Clone struct {
	Git *CloneStep `yaml:"git,omitempty"`
	Hg  *CloneStep `yaml:"hg,omitempty"`
}

type Pipeline struct {
	Name      string            `yaml:"-"`
	Platform  string            `yaml:"platform,omitempty"`
	Labels    map[string]string `yaml:"labels,omitempty"`
	RunsOn    []string          `yaml:"runs_on,omitempty"`
	Branches  *Conditions       `yaml:"branches,omitempty"`
	DependsOn []string          `yaml:"depends_on,omitempty"`
	Services  Services          `yaml:"services,omitempty"`
	Workspace *Workspace        `yaml:"workspace,omitempty"`
	SkipClone bool              `yaml:"skip_clone,omitempty"`
	Clone     *Clone            `yaml:"clone,omitempty"`
	Steps     Steps             `yaml:"pipeline"`
}
