// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package drone

import (
	"codeberg.org/lafriks/woodpecker-pipeline-transform/core"
)

type Conditions struct {
	Conditions core.Strings `yaml:"-"`
	Include    core.Strings `yaml:"include,omitempty"`
	Exclude    core.Strings `yaml:"exclude,omitempty"`
}

func (c *Conditions) IsEmpty() bool {
	if c == nil {
		return true
	}
	return len(c.Conditions) == 0 && len(c.Include) == 0 && len(c.Exclude) == 0
}

func (c *Conditions) UnmarshalYAML(unmarshal func(interface{}) error) error {
	v := &struct {
		Include core.Strings `yaml:"include,omitempty"`
		Exclude core.Strings `yaml:"exclude,omitempty"`
	}{}
	if err := unmarshal(v); err == nil {
		if c == nil {
			c = &Conditions{}
		}
		c.Include = v.Include
		c.Exclude = v.Exclude
		return nil
	}
	v2 := make(core.Strings, 0)
	if err := unmarshal(&v2); err != nil {
		return err
	}
	if c == nil {
		c = &Conditions{}
	}
	c.Conditions = v2
	return nil
}

type When struct {
	Branch       *Conditions  `yaml:"branch"`
	Event        core.Strings `yaml:"event"`
	Refs         *Conditions  `yaml:"refs"`
	Repositories *Conditions  `yaml:"repo"`
	Instance     *Conditions  `yaml:"instance"`
	Status       core.Strings `yaml:"status"`
	Target       *Conditions  `yaml:"target"`
	Cron         *Conditions  `yaml:"cron"`
}

type Trigger struct {
	Branch       *Conditions  `yaml:"branch"`
	Event        core.Strings `yaml:"event"`
	Refs         *Conditions  `yaml:"refs"`
	Repositories *Conditions  `yaml:"repo"`
	Status       core.Strings `yaml:"status"`
	Target       *Conditions  `yaml:"target"`
	Cron         *Conditions  `yaml:"cron"`
	Action       *Conditions  `yaml:"action"`
}
