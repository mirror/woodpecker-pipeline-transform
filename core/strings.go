// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package core

// Strings represents a list of strings.
type Strings []string

// UnmarshalYAML implements the Unmarshaler interface that accepts either single string or array of strings.
func (s *Strings) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var arr []string
	if err := unmarshal(&arr); err == nil {
		*s = Strings(arr)
		return nil
	}
	var v string
	if err := unmarshal(&v); err != nil {
		return err
	}
	*s = Strings{v}
	return nil
}

// MarshalYAML implements the Marshaler interface that will write string for single value or array of strings.
func (s Strings) MarshalYAML() (interface{}, error) {
	if len(s) == 1 {
		return s[0], nil
	}
	return []string(s), nil
}
