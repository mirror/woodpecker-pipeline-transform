// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"

	"codeberg.org/lafriks/woodpecker-pipeline-transform/drone"
	"github.com/goccy/go-yaml"
	"github.com/spf13/cobra"
)

var (
	pipelineType string
	sourcePath   string
	destPath     string
)

func ReadSource(path string) ([]*transform.Source, error) {
	fs, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if !fs.IsDir() {
		buf, err := os.ReadFile(path)
		if err != nil {
			return nil, err
		}
		return []*transform.Source{
			{
				Name:    fs.Name(),
				Content: buf,
			},
		}, nil
	}
	sources := make([]*transform.Source, 0)
	dir, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}
	for _, f := range dir {
		if f.IsDir() {
			continue
		}

		buf, err := os.ReadFile(filepath.Join(path, f.Name()))
		if err != nil {
			return nil, err
		}
		sources = append(sources, &transform.Source{
			Name:    f.Name(),
			Content: buf,
		})
	}
	return sources, nil
}

func WriteDest(tr transform.Transformer, pipelines []*transform.Pipeline, path string) error {
	if len(pipelines) == 0 {
		return nil
	}

	fs, err := os.Stat(path)
	if (err != nil || !fs.IsDir()) && len(pipelines) > 1 {
		return errors.New("source has multiple pipelines but destination path is not a directory")
	}
	if len(pipelines) > 1 {
		if fs.Name() != ".woodpecker" {
			path = filepath.Join(path, ".woodpecker")
		}
		if err = os.MkdirAll(path, 0o755); err != nil {
			return err
		}
	}

	for _, pipeline := range pipelines {
		p := path
		if len(pipelines) == 1 {
			p = filepath.Join(p, ".woodpecker.yml")
		} else {
			p = filepath.Join(p, "."+pipeline.Name+".yml")
		}
		buf, err := yaml.Marshal(pipeline)
		if err != nil {
			return err
		}
		buf, err = transform.FormatWoodpeckerYAML(buf)
		if err != nil {
			return err
		}
		buf, err = tr.PostProcess(buf)
		if err != nil {
			return err
		}
		if err = os.WriteFile(p, buf, 0o644); err != nil {
			return err
		}
	}

	return nil
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "pipeline-convert",
	Short: "Convert a pipeline from one format to another",
	Long: `Convert a pipeline from one format to another.
Currently supports converting from Drone CI pipelines to Woodpecker CI pipeline format.`,
	Run: func(cmd *cobra.Command, args []string) {
		if pipelineType != "drone" {
			fmt.Println("Error: unsupported pipeline format type")
			os.Exit(1)
		}
		if sourcePath == "" {
			fmt.Println("Error: source path is required")
			_ = cmd.Help()
			os.Exit(1)
		}
		if destPath == "" {
			fmt.Println("Error: destination path is required")
			_ = cmd.Help()
			os.Exit(1)
		}
		sources, err := ReadSource(sourcePath)
		if err != nil {
			fmt.Printf("Error: failed to load source: %v\n", err)
			os.Exit(1)
		}
		p := drone.New()
		pipelines, err := p.Transform(sources)
		if err != nil {
			fmt.Printf("Error: failed to convert pipeline: %v\n", err)
			os.Exit(1)
		}
		if err = WriteDest(p, pipelines, destPath); err != nil {
			fmt.Printf("Error: failed to write converted pipeline to destination: %v\n", err)
			os.Exit(1)
		}
	},
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVarP(&pipelineType, "type", "t", "drone", "pipeline format type (supported: drone)")
	rootCmd.Flags().StringVarP(&sourcePath, "source", "s", "", "source pipeline file/dir path")
	rootCmd.Flags().StringVarP(&destPath, "dest", "d", "", "destination pipeline file/dir path")
}
